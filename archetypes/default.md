+++
title = "{{ replace .TranslationBaseName "-" " " | title }}"
description = ""
author = ""
date = {{ .Date }}
draft = true
image = "" # for hero image
images = [] # for open graph
tags = []
+++
