+++
tags =  ["books", "reading"]
date = "2016-01-17T23:24:12-06:00"
description = "Finished Better Angels and on to new books"
draft = false
images = ["http://josephstahl.com/media/WhatImReadingJanuary2016/heropicture.jpg"]
title = "What I'm Reading, January 2016"

+++

![GEB, Sapiens, and How to Win Friends and Influence People](/media/WhatImReadingJanuary2016/heropicture.jpg)

## Summertime

I'm reminded now that I haven't written anything since I returned from Vienna.
Other than a trip to Maryland to visit a friend, I spent the remainder of my
summer in Birmingham. Traveling on a Maymester meant I did not have the 10 weeks
available that most internships take so I turned my focus to reading. Certainly
the highlight of my summer was finishing reading Steven Pinker's _The Better
Angels of Our Nature: Why Violence Has Declined_, a massive explanation of the
sometimes-hard-to-spot decline in violence throughout the world.

<!--more-->

### Thoughts on Better Angels

First of all, a disclaimer: I finished this book several months ago and left it
in Birmingham so much of this is written with the help of the book's Wikipedia
article. For those unfamiliar with the book, Pinker writes:

> violence has been in decline over millennia and that the present is probably
> the most peaceful time in the history of the human species. The decline in
> violence, he argues, is enormous in magnitude, visible on both long and short
> time scales, and found in many domains, including military conflict, homicide,
> genocide, torture, criminal justice, and treatment of children, homosexuals,
> animals and racial and ethnic minorities. \[1\]

To say his analysis is thorough would be an understatment; Pinker's tour de
force left me no doubt that the world is moving to a more peaceful time.
Granted, there are setbacks and areas progressing slower than others but overall
there is no better time to live than the present. To emphasize the amount of
interesting material in this book: it was the first I have read outside of class
with a pencil for underlining. Without the book recalling my favorite examples
and thoughts is difficult but a few come to mind:

- Animal's rights leading to children's rights: as I recall in the book, Pinker
  points out how codifying protection of animals eventually came to benefit
  children as courts used animal rights laws to protect children. An interesting
  story about how progress happens in unexpected ways.
- Poisson Distribution: I read this part of the book before I took a statistics
  class so the idea of a Poisson Distribution was both interesting and confusing
  to me. I stayed struck on the two pages explaining this topic for about thirty
  minutes. Pinker introduces the idea with an example: Consider a house that is
  struck by lightning once every 7 days, on average. If lightning strikes on
  Sunday, when is the next strike most likely to occur? I initially thought the
  next strike would most likely occur in 7 days, on the following Sunday.
  However, this instinct is **incorrect**! Rather, the next strike is most
  likely to occur the very next day, Monday. Why? Because lightning is a random
  event with no memory. Thus, for a lighting strike to occur 7 days later, it
  must _not_ occur for 6 days. An obvious fact, sure, but one which greatly
  affects the problem. Because if the lightning has a 1/7 chance of striking any
  day, it has a 6/7 chance of not striking a given day. So to strike 7 days
  later, the probability is (6/7)^6 \* (1/7). Whereas to strike the very next
  day, the probability is 1/7. Similarly, looking at recent history and seeing a
  cluster of wars may cause one to believe that they are frequent when in fact
  they are random. The clustering does not matter as long as the chance of a war
  breaking out continues to decrease—which it does.
- American South more violent than the North: this fact interested me, being
  from the South. Based on my personal experience, Pinker is exactly right with
  his explanation: the South has cultivated a culture of honor and individualism
  while the North is more community oriented. Moreover, the South has
  historically lagged behind the North in terms of government reach, leading
  some to believe that they must take justice into their own hands to correct
  for perceived wrongs. Combining a culture that values honor with one that is
  quick to retaliate for violations of that honor creates a predicatably
  violence-inclined region.

Overall, I cannot recommend the book highly enough. The book, despite its
length, remains relatively easy to read; Pinker writes in a clear manner that
makes his ideas easy to follow. The book resembles a standard novel in
readability far more than the average textbook.

### What's Next

I am currently reading Douglas Hofstadter's _Gödel, Escher, Bach: an Eternal
Golden Braid_. Like Better Angels, this is one I have to keep a pencil with me
while reading. I started reading the book and am just past page 100 (progress
has slowed with classes resuming) but am enjoying it greatly. The book has done
a great job of building upon itself and the stories, pictures, puzzles, etc. do
a good job of keeping my attention and introducing new ideas.

Underneath GEB lies Yuval Noah Harari's _Sapiens: A Brief History of Humankind_,
a book which caught my attention after a recommendation via Twitter. I've
noticed a trend in my reading history: massive, sweeping works tend to grab my
attention, whether an explanation for decreased violence, an investigation into
how the mind works, or in the case of this book, a history of humankind from the
rise of _homo sapiens_ to present times with special attention to the various
revolutions (Cognitive, Agricultural, and Scientific) that have changed the
course of human history. I'm excited to read it although considering that Better
Angels took me more than a year to finish I think Sapiens will stay unopened for
a while longer as I focus on GEB.

I also ordered Dale Carnegie's _How To Win Friends and Influence People_, an old
(albeit updated) book that seeks to do exactly what its title suggests. With
medical school admissions and interviews coming up I thought a book could
provide help with making the best possible impression on the various people that
can literally change my life in a few minutes. The book so far hasn't been
groundbreaking but I do feel like it has been beneficial.

If anyone has reading suggestions for me, email me! Right now I'm looking for
lighter pleasure reading; my Operating Systems textbook last semester actually
had a good-looking selection of the authors' recommended fiction books which
caught my attention.

### References

[\[1\]](https://en.wikipedia.org/wiki/The_Better_Angels_of_Our_Nature)
