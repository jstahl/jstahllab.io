+++
title = "20th Grade"
description = "1 more year of med school!"
author = "Joseph Stahl"
date = 2020-07-01T18:03:07-05:00
draft = false
image = "" # for hero image
images = [] # for open graph
tags = ["update"]
+++

# My last year of medical school!

Turns out I was right about
[posting less in med school](/blog/2017/08/05/the-next-4-years/)! It's been a
blur the past 3 years but I've enjoyed (nearly) every moment of it—10 weeks of
online classes and not being allowed in the hospital due to COVID-19 being the
notable exception. I've made some amazing friends, continue to learn an
incredible amount, and I absolutely love being in the OR!

## So what's next?

(Applying to) general surgery residency! And in the meantime, I've got some
acting internships that I can't wait to start!
