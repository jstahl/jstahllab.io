+++
description = "An update on the past year and what's coming up"
title = "January 2017 Update"
date = "2017-01-20T15:19:44-06:00"
tags = ["travel", "update"]
author = "Joseph Stahl"
images = ["https://josephstahl.com/media/January2017Update/NYC.jpg"]
draft = false
+++

Now that there's a new theme and updated resume on the website, it's time for a
new blog post. Considering that my last post was a year ago, there's plenty to
share!

## Looking back on 2016

### Books

Still reading Godel, Escher, Bach—no surprise there. It's a dense book, but I
have made progress on several others. I finished _Real Food/Fake Food: Why You
Don't Know What You're Eating_ by Larry Olmstead, and promptly went out and
bought a piece of real Parmesan cheese. Good stuff.

Most recently, I've started _Hero of the Empire: The Boer War, a Daring Escape,
and the Making of Winston Churchill_ by Candice Millard. I'm about halfway
through the book and enjoying it immensely; Millard does an excellent job of
recounting Churchill's life before his rise to Prime Minister.

I took two English classes in 2016—one on sexual violence, one on South Asia—and
read a number of books in each. Mohsin Hamid's writing (fiction set in South
Asia) stood out as particularly appealing; his style is straightforward and
terse without losing any of it's narrative power.

### Academics

Discussing English brings me to my classes in 2016; I finished 2016 extremely
pleased with my performance. In Spring 2016, I finished my final "core"
pre-medical requirements at Vanderbilt with the completion of Organic Chemistry.
With only one pre-med class that semester, and none in Fall 2016, I finally felt
like I was no longer being pulled in two separate directions—I've learned the
hard way why so few people combine Computer Science with a pre-med curriculum.

As I get closer to graduating, I find myself frequently answering the question
"Why did you do Computer Science and pre-med?" The truth of the matter is that
if I could do this over, I'm unsure of whether I would combine them. Growing up,
my parents frequently told me to "do what you love". With that in mind, I
entered Vanderbilt as a Computer Science major and soon made up my mind to
pursue medicine as well. The fact that I enjoy both programming and the "hard
sciences" required for medical school—chemistry, biology, biochemistry, etc.—has
undoubtedly helped me to make it through a difficult curriculum in four years
with a GPA I am proud of. However, there are many paths that I suspect would
have been easier; majors where my required classes would more closely overlap
with the classes I had to take for medical school. For example, I will have
taken 10 "hard science" classes; I believe that a CS major is only required to
take 2. Similarly, I will have taken 5 math classes here; medical schools
require at most a single semester of calculus. The disparate of requirements of
computer science and medicine mean that by May 2017, I will have taken exactly 2
classes for fun—classes unrelated to medicine or computer science.

But I can't go back in time, so I try not to think about what I would do
differently if I could. I've enjoyed my classes and have become immenesely more
knowledgeable about a variety of topics, and there's not much more I could ask
for in a college education. Now to count down to May 2017! :tada:

### Travel

I took a few trips in 2016, some for interviews and some for fun. Spring break
took me to the beach house in Florida for a week; other than that, all of spring
semester was in Nashville. A summer biochemistry class and MCAT prep class kept
me in Birmingham for most of the summer but I had a few weeks off at the
beginning. I started my summer at the Kentucky Derby (turns out I'm terrible at
betting on horses) before heading back to Birmingham and later to Florida.

![Kentucky Derby](/media/January2017Update/Derby.jpg "Main gate at Churchill Downs")

Thanks to the air conditioning breaking at the beach house, I spent much of that
time alone in Florida, house-sitting and studying review books while it was
fixed. This sounds worse than it actually was; the temperatures had not yet
gotten too bad and I couldn't have asked for a better place to study than an
entire house to myself.

In mid-August, I flew to NYC; mostly to celebrate a friend's birthday but also
to celebrate finishing the MCAT. As usual, whenever I arrive in New York it's a
mixture of awe and timidity; it takes me a day or two each time to reacquaint
myself with the pace, transportation, people, and everything else that's so
different in New York.

![Me in NYC](/media/January2017Update/NYC.jpg "Me in NYC")

By late August, it was time to head back for band camp and the fall semester.
Medical school interviews kept me rather busy in the fall, although I still
found time to make it to the beach over fall break. As I'm still in the
interview process I shouldn't say where I've interviewed, but I can say I am
immensely thankful to my parents for all their help with my travels.

Vanderbilt football's unexpected win over Tennessee sent me to the Camping World
Independence Bowl in Shreveport, LA on December 26 with the marching band.
Overall, the trip was a great way to finish 2016, although it wasn't without its
rough spots. Due to a misunderstanding over what to leave on the bus, I almost
left my saxophone, as well as a friend's saxophone, in Nashville. We had a
chartered flight to Shreveport on Christmas Day and I spent the entire flight
thinking that the saxophones had been left in Nashville; however, when we landed
I saw them—the first two items to come off the plane. Unbeknownst to me, the bus
had turned around and made it back to the tarmac during the preflight safety
briefing, giving the ground crew just enough time to get the saxophones on the
plane. Proving that the Vanderbilt band is the best band (to be in) in the SEC,
we were allowed to go to the local casinos that night, where I learned I have
more luck with slots than with betting on horses.

<blockquote class="twitter-tweet" data-conversation="none" data-lang="en"><p lang="en" dir="ltr">Update: Christmas break: Nashville✈New York City✈Nashville🚗Birmingham🚗Nashville✈️Shreveport(⚓⬇)✈️Nashville✈PCB🚗Birmingham🚗Nashville. Wow</p>&mdash; Joseph Stahl (@josephst18) <a href="https://twitter.com/josephst18/status/808840655975907328">December 14, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

### Pre-med stuff

2016 was **by far** the busiest year in my pre-med journey. When the year
started, I had no idea where I would apply, when I would take the MCAT, or what
I would write about in my personal statement. By the start of 2017, I had my
first acceptance! I started my AMCAS application in early 2016 and signed up for
an August MCAT date, knowing that a later date would allow me to study for the
MCAT and take Biochemistry over the summer. I wrote my personal statement after
finishing classes and submitted my finalized AMCAS application two weeks after
submissions opened. I took the MCAT on August 5, then turned my attention to
finishing the various secondary applications I was receiving. There are still a
few loose ends on interviews and paperwork but it's nothing now compared to how
busy I was at the height of the application process. Advice to anyone applying:
start early, stay confident, and **stay organized**.

### Computer science stuff

On a spur-of-the-moment decision, I applied to the Google Summer of Code
program, thinking that part-time remote development would be the perfect match
for spending my summer between Birmingham and Florida, studying for the MCAT and
learning Biochemistry. To my mild surprise (given my impromptu decision to
apply), my application was accepted and I started to write tutorials for
[Cytoscape.js](http://js.cytoscape.org). My final posts are on
[blog.js.cytoscape.org](http://blog.js.cytoscape.org/) and cover an
introduction, animating graphs, and depicting a social-network-type graph, first
on the browser, then using Electron on the desktop, then using Cordova on
mobile.

In school, I had the chance to branch out from requried CS classes and choose
classes more closely aligned with my own interests. In Spring 2016 I took a Big
Data class, which culminated in a group project where I used PySpark and
SciKit-Learn to analyze economic data to predict recessions. The project dealt
with economic data, but it's easy to see how the same software could easily look
at health data for predicting disease risks to aid in prevention and early
intervention.

In Fall 2016 I took a Web Development class, which left me thankful for my
summer spent working with Cytoscape.js and learning JavaScript. Although we
spent some time discussing HTML and CSS, they were only a miniscule part of the
class with the majority dedicated to "all JavaScript, all the time". I spent the
semester building a Solitaire application, first in jQuery and then moving it
over to React. It was by far the most involved class (in terms of homework) I
had in Fall 2016 but gave me a chance to learn new ES6 Javascript syntax,
Webpack, Node.js, Jade, using Bootstrap for CSS, jQuery, React, Babel, and
probably a few more technologies I'm forgetting.

I'd describe 2016 as the year that my CS classes went from homework to
projects—fewer and larger assignments, and a much more realistic model of
real-world software development.

## Looking forward to 2017

### Running

I slowly increased my weekly milage in 2016 and decided in December that I would
aim for my first half-marathon. I'm running in the Nashville Rock-n-Roll
Half-Marathon on April 27! It's still 4 months away, so training hasn't picked
up too much for it yet; however, I did a 10.4 mile run last weekend so I'm
feeling pretty confident about being able to do 13.1 by April.

### Continuing with Cytoscape.js

I've continued to work with Cytoscape.js following the end of my GSoC project;
in late 2016 I first wrote a set of slides with
[reveal.js](https://github.com/hakimel/reveal.js/) and embedded Cytoscape.js
graphs and am now working on finishing some small features for the v3.1 release.

### Independent study

Now that I've had time to get familiar with Cytoscape.js I am working on my own
project. It turns out that the Canadian government funds a website called
[DrugBank](https://www.drugbank.ca/), which contains over 8000 entries of
various pharmaceutical drugs. All interactions between drugs are also listed,
which makes the data set great for graphing—I envision being able to search for
or click on a drug, then having all other drugs which interact with the selected
drug highlighted and being able to read about the interactions. I think it'll be
a great tool for education, plus it's an excellent way to demonstrate one of the
many ways that computer science and medicine can be integrated.

### Chemistry minor

As mentioned above, my college curriculum has often left me feeling like I'm
trying to be two different people at once—Joseph the scientist and Joseph the
developer—so I figured I might as well embrace it and go for a minor. After
finishing Biochemistry I was only one class away from the minor, so I'm
finishing up my college career with Bioinorganic Chemistry to receive a
chemistry minor.

### Medical school

One acceptance so far, with the process still ongoing. It's quite the process
and one I don't want to talk about too much while I'm still in the middle of it.
More details coming later. Hopefully I can write another post before
January 2018.

### Graduating

Hard to believe I'm almost done at Vanderbilt! Things have gone by in a hurry; I
still remember moving into Hank Ingram House in August 2013 and now I've already
made plans for my graduation brunch. Right now it's looking like I'll graduate
in May and have a few weeks off before beginning my next adventure: medical
school!
