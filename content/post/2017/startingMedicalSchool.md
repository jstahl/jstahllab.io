+++
author = "Joseph Stahl"
date = "2017-08-05T17:02:46-05:00"
description = "I'm starting medical school!"
tags = ["update"]
title = "The Next 4 Years"
aliases = [
  "blog/2017/08/05/starting-medical-school"
]
images = ["https://josephstahl.com/media/startingmedicalschool/ocean.jpg"]
image = "https://josephstahl.com/media/startingmedicalschool/ocean.jpg"
+++

I start classes at the University of South Alabama College of Medicine on
Monday, August 7th! I'm excited to kick off these next four years at a fantastic
institution, surrounded by amazing classmates.

In other words, blog posts will likely become (even more) sparse as I focus on
classes.
