+++
title = "2017 in Review"
description = "A graduation, 1640 miles of running, and more!"
author = "Joseph Stahl"
date = 2017-12-20T01:14:13-06:00
draft = false
image = "/media/2017InReview/Graduation2.jpg" # for hero image
images = ["/media/2017InReview/Graduation2.jpg", "/media/2017InReview/Graduation.jpg"] # for open graph
tags = ["update"]
+++

# Intro

Numbers. 2017 had a lot of them! After 147 credit-hours, I graduated from
Vanderbilt; I ran 1640 miles; and a whole lot more! Now, let's do the numbers:

<!-- more -->

## 147 hours taken

After 147 hours of classes over 4 years at Vanderbilt University: I graduated!
Finished my major in Computer Science, plus my pre-med curriculum was enough to
get me a minor in Chemistry. I had four fantastic years there and looking back,
I definitely made the right choice in where to go.

## 1640.9 miles ran

![Not too photogenic after my first race](/media/2017InReview/PostRace.jpg)
_1:41:29 and looking exhausted!_

Around Thanksgiving of 2016, I decided to work on increasing my running mileage
and run in the Nashville Rock & Roll Half Marathon in May 2017. Finished with a
1:41:29 chip time, good for 236th out of 17886 runners total! Logging everything
on [Strava](https://www.strava.com/athletes/11507482) has made it easy to see my
mileage (and get reminders when I've worn through another pair of shoes). Year's
not over yet, but right now it looks like I'm on track to cross the 1600 mile
mark with over 200 hours spread out over 200 runs. Doing the math on that, it's
apparent that a lot of my runs are 1hr, 8 mile runs. A good distance, yes, but I
need to work on adding more variety to my runs in 2018—tempo runs, pace runs,
etc.

New goals: running one leg of a relay in Mobile, then I'll be in Birmingham for
the Mercedes (Half) Marathon! Here's to hoping I can break the 1:35:00 mark!

![Centennial Park, during a run](/media/2017InReview/Centennial.jpg) _Centennial
Park during one of my half-marathon training runs_

## 4 pairs of running shoes, worn out

Went through 4 pairs of running shoes during 2017—and it probably should have
been more if I was better about replacing them every ~400 miles.

## 1 semester, done

Done with my first semester at medical school! It's been a success so far;
challenging but not impossible and it feels great to be one step closer to a
career in medicine.

## 52,000 miles driven

After 3 years of driving between Nashville and Birmingham (plus 1 year of being
driven—thanks Mom!) and a half year driving between Mobile and Birmingham, my
car is now up to about 52k miles. Slowly creeping up (and I'm now a pro at
driving I-65).

_Yes, I know this wasn't 52,000 miles this year. Never kept track of this before
so we're going cumulative here!_

## 52,753 minutes of music

![Spotify's 2017 Wrapped](/media/2017InReview/SpotifyMusicHistory.png) Spotify
tells me I listened to 52,753 minutes of music (with 3,790 different songs).
Having an internet-connected speaker inflates that number a lot; unless I'm
studying, there's music playing in my apartment. Conveniently, Spotify gives me
a breakdown of my top categories:

- Top Artists
  - [The xx](https://open.spotify.com/artist/3iOvXCl6edW5Um0fXEBRXy)
  - [Arctic Monkeys](https://open.spotify.com/artist/7Ln80lUS6He07XvHI8qqHH)
  - [CHVRCHES](https://open.spotify.com/artist/3CjlHNtplJyTf9npxaPl5w)
  - [My Chemical Romance ](https://open.spotify.com/artist/7FBcuc1gsnv6Y1nwFtNRCb)
- Top Songs
  - [The xx - Brave For You](https://open.spotify.com/track/53e5O0USYgSCv4md0CXpMY)
  - [Jimmy Eat World - Disintegration](https://open.spotify.com/track/7jqlpEgPLAYpj7ZfESA294)
  - [The xx - Test Me](https://open.spotify.com/track/3qLwtC9jEWugjkzyZlYgeg)
  - [Marilyn Manson - Killing Strangers](https://open.spotify.com/track/4gacG0EFSqvgVXEbPX8gCY)
  - [Fix You - Coldplay](https://open.spotify.com/track/7LVHVU3tWfcxj5aiPFEW4Q)
- What I'm Listening To Now
  - [Broods - Conscious](https://open.spotify.com/track/5aEVumHjIBObFBfNLWTrUV)
  - [Wolf Alice - Silk](https://open.spotify.com/track/5z75fXqSd5jhRsV543Trxh)
  - [Arcade Fire - Put Your Money On Me](https://open.spotify.com/track/0SaEmR2rdtfsZawPjMYkWg)
  - [The Neighbourhood - Sadderdaze](https://open.spotify.com/track/31vZNE9IbY0JI0gRmaLAWr)
  - [Sir Sly - &Run](https://open.spotify.com/track/0w4m3Xp4svlIoyL94Zjlyd)

## 1 eclipse, witnessed

![Eclipse](/media/2017InReview/Eclipse.jpg)

Not in the path of totality, but nevertheless a cool experience.

# Looking Ahead

2018 is coming up quick! At the top of my list for 2018 is continuing to perform
well in medical school as I finish my M1 and move into my M2 year. As for
running, 2017 was the year I realized I could run a half-marathon relatively
quickly; I'm aiming to make 2018 the year to see just how fast I can get. I've
never liked doing speed work, but it's going to be necessary to bring my time
down. I'd love to say I'm planning to travel more in 2018, but it's hard to find
the time with medical school and research taking up large portions of my year.
As for smaller goals, I want to read more and work on open-source projects over
holidays and breaks.

Overall, 2018 is looking to be a lot less eventful than 2017—it's hard to
compete with 2017, considering that I graduated and started medical school. Time
to wrap up 2017 and ring in the new year! 🎆

![Graduation!](/media/2017InReview/Graduation.jpg)
