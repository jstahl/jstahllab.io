+++
title = "Tegrity Downloader"
description = "A small bookmarklet to download videos from Tegrity"
author = "Joseph Stahl"
date = 2018-04-15T22:01:54-05:00
draft = false
image = "" # for hero image
images = [] # for open graph
tags = []
+++

# Tegrity Video Downloader

## Quick install

Drag the link below into your address bar, open a Tegrity video, and click it.
The video should open in a new window, ready for download with Ctrl-S.

<a href="javascript:(function()%20{%20let%20playerUrl%20=%20document.getElementById('playerContainer').attributes['iframe-src'].value;%20%20let%20videoUrl%20=%20playerUrl.replace('TegrityPlayer.htm',%20'Projector/screen00.mp4');%20videoUrl%20=%20videoUrl.substring(0,%20videoUrl.indexOf('?'));%20%20let%20authTokenStart%20=%20playerUrl.indexOf('AuthToken');%20let%20authTokenEnd%20=%20playerUrl.indexOf('&',%20authTokenStart);%20let%20authToken%20=%20playerUrl.substring(authTokenStart,%20authTokenEnd);%20%20let%20authVideoUrl%20=%20videoUrl%20+%20'?'%20+%20authToken;%20%20console.log('Found%20video%20URL:%20'%20+%20authVideoUrl);%20window.open(authVideoUrl);%20})();">Download
Tegrity</a>

## More details

Check out the
[Github Gist](https://gist.github.com/josephst/34179a7ec1cccaf9efec57a1603d8af0)
to see the code behind this (and lengthier install instructions)

Current version: 0.1

## The code

For the curious, here's the code that powers this bookmarklet

<script src="https://gist.github.com/josephst/34179a7ec1cccaf9efec57a1603d8af0.js?file=script.js"></script>

## Copyright — MIT License

```
Copyright 2018 Joseph Stahl <hello (at) josephstahl (dot) com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
