+++
title = "Cocktail List"
description = "A perpetually incomplete list of drinks and how to make them"
author = "Joseph Stahl"
date = 2020-07-01T18:21:31-05:00
draft = false
image = "" # for hero image
images = [] # for open graph
tags = []
+++

To be honest, I forgot that my little React
[Cocktail List](https://cocktails.josephstahl.com/drinks) app was still online.

It's been 2+ years since I updated it. At this point, it's a cautionary tale in
when it's best to just write plain HTML (which is way more maintainable than the
React + TypeScript + MobX setup that I'm currently struggling to update).

But if you ever need to know how to make a simple gin and tonic, here you go!
