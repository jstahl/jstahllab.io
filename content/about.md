+++
date = "2014-01-01T20:41:11-06:00"
draft = false
title = "About Me & Contact Info"

+++

I'm a medical student at the University of South Alabama College of Medicine.
Previously at Vanderbilt University (BS Computer Science, 2017). I'm experienced
in C++, Java, JavaScript, and Python, with a particular interest in JavaScript
and TypeScript development. When I'm not studying or coding, I'm probably
running. Especially if it means getting to explore a new city. I'm a big fan of
saxophone, piano, and science fiction. Both a dog and a cat person.

---

### Get in touch

👋 hello at josephstahl (dot) com

[Twitter](https://twitter.com/thatjosephstahl)

[josephstahl.com](https://josephstahl.com)
